#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
PARENT_FOLDER=$(realpath $(dirname ${SCRIPT_FOLDER})/..)
PROJECT_NAME=$(basename ${PARENT_FOLDER})

echo "SCRIPT_FOLDER: ${SCRIPT_FOLDER}"
echo "PARENT_FOLDER: ${PARENT_FOLDER}"
echo "PROJECT_NAME: ${PROJECT_NAME}"

helm template ${PARENT_FOLDER}/helm
helm upgrade --install --wait \
    --set image.tag=registry.gitlab.com/innoq/container-e2e-demo/${PROJECT_NAME}/master:$(git rev-parse HEAD) \
    --set ingress.host=container-e2e-demo-on-aws.ses.innoq.io \
    ${PROJECT_NAME} ${PARENT_FOLDER}/helm